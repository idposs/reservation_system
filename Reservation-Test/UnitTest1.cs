﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReservationSystem;

namespace Reservation_Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            SimpleMethodforUnitTest s = new SimpleMethodforUnitTest();
            int actual=s.Sample(2, 3);
            int expected=5;
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethod2()
        {
            SimpleMethodforUnitTest s = new SimpleMethodforUnitTest();
            int actual = s.Sample(4,5);
            int expected = 9;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethod3()
        {
            SimpleMethodforUnitTest s = new SimpleMethodforUnitTest();
            int actual = s.Sample(0, 0);
            int expected = 0;
            Assert.AreEqual(expected, actual);
        }
    }
}
