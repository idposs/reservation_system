﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ReservationSystem.LoginClass"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
        
    <style type="text/css">
        .style5
        {
            width: 180px;
            text-align: right;
        }
        .style6
        {
            width: 180px;
            text-align: center;
            color: #663300;
        }
        .style7
        {
            width: 188px;
            text-align: center;
        }
        .style8
        {
            width: 175px;
            text-align: right;
        }
        .style9
        {
            width: 268px;
            text-align: right;
        }
        .style10
        {
            text-align: left;
        }
        .style11
        {
            width: 188px;
            text-align: center;
            color: #663300;
            font-size: large;
        }
    .style12
    {
        width: 175px;
        text-align: right;
        height: 59px;
    }
    .style13
    {
        width: 268px;
        text-align: right;
        height: 59px;
    }
    .style14
    {
        width: 180px;
        text-align: right;
        height: 59px;
    }
    .style15
    {
        width: 188px;
        text-align: center;
        height: 59px;
    }
    .style16
    {
        text-align: left;
        height: 59px;
    }
    .style17
    {
        height: 59px;
    }
    .style18
    {
        width: 175px;
        text-align: right;
        height: 34px;
    }
    .style19
    {
        width: 268px;
        text-align: right;
        height: 34px;
    }
    .style20
    {
        width: 180px;
        text-align: right;
        height: 34px;
    }
    .style21
    {
        width: 188px;
        text-align: center;
        height: 34px;
    }
    .style22
    {
        text-align: left;
        height: 34px;
    }
    .style23
    {
        height: 34px;
    }
    </style>
   
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    
    
     <table style="width: 100%; height: 388px;" >
            <tr>
                <td class="style8">
                    &nbsp;</td>
                <td class="style9">
                    &nbsp;</td>
                <td class="style6">
                    &nbsp;</td>
                <td class="style11">
                    <strong>Login</strong></td>
                <td class="style10">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style8">
                    &nbsp;</td>
                <td class="style9">
                    &nbsp;</td>
                <td class="style6">
                    &nbsp;</td>
                <td class="style7" nowrap="nowrap">
                    <asp:Label ID="lbl_GoodBye" runat="server" ForeColor="#CC33FF"></asp:Label>
                </td>
                <td class="style10">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style12">
                    </td>
                <td class="style13">
                    </td>
                <td class="style14">
                    <strong>Enter the User Name&nbsp;&nbsp; :</strong></td>
                <td class="style15">
                    <asp:TextBox ID="txt_User" 
        runat="server" EnableViewState="False" Height="22px" Width="128px"></asp:TextBox>
                </td>
                <td class="style16">
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ControlToValidate="txt_User" ErrorMessage="Username can not be blank" 
        ForeColor="Red" ValidationGroup="vg"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style8">
                    &nbsp;</td>
                <td class="style9">
                    &nbsp;</td>
                <td class="style5">
                    <strong>Enter the Password&nbsp;&nbsp; :</strong></td>
                <td class="style7">
                    <asp:TextBox ID="txt_Pwd" 
        runat="server" EnableViewState="False" Height="22px" Width="128px" TextMode="Password"></asp:TextBox>
                </td>
                <td class="style10">
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="txt_pwd" ErrorMessage="Password can not be blank" 
            ForeColor="Red" ValidationGroup="vg"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style8">
                    &nbsp;</td>
                <td class="style9">
                    &nbsp;</td>
                <td class="style5">
                    <br />
                </td>
                <td class="style7">
    <asp:Button ID="btn_Login" runat="server" onclick="btn_Login_Click" 
        Text="Login" ValidationGroup="vg" />
                    <br />
        <asp:Label ID="lbl_LoginError" runat="server"></asp:Label>
                </td>
                <td class="style10">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style8">
                    &nbsp;</td>
                <td class="style9">
                    &nbsp;</td>
                <td class="style5">
        <asp:LinkButton ID="lb_Signup" runat="server" 
            PostBackUrl="~/RegistrationPage.aspx">Signup</asp:LinkButton>
                </td>
                <td class="style7">
                    &nbsp;</td>
                <td class="style10">
        <asp:LinkButton ID="lb_ForgotPassword" runat="server" 
            PostBackUrl="~/PasswordRecoveryPage.aspx">Forgot Password?</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td class="style18">
                    </td>
                <td class="style19">
                    </td>
                <td class="style20">
                    </td>
                <td class="style21">
                    &nbsp;</td>
                <td class="style22">
                    </td>
            </tr>
        </table>     
        
</asp:Content>
