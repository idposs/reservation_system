﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace ReservationSystem
{
    public partial class LoginClass : System.Web.UI.Page
    {
       //pavan oruganti
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                txt_User.Text = null;
                if (Request.QueryString["event"] == "SignOut")
                {
                    lbl_GoodBye.Attributes.Add("Style", "text-decoration:blink");
                    lbl_GoodBye.Text = "Thank you for using infy-reservation system.See you soon!!!!!";
                }                
                
            }            

        }

        protected void btn_Login_Click(object sender, EventArgs e)
        {
            
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["ReservationDBConnectionString"].ConnectionString;
            string query = "select count(*) from Registration where userid=@uid and password=@pwd";
            string query2 = "select EmailId from Registration where userid=@uid";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlCommand cmd2 = new SqlCommand(query2, conn);
            cmd.Parameters.AddWithValue("@uid", txt_User.Text);
            cmd.Parameters.AddWithValue("@pwd", txt_Pwd.Text);
            cmd2.Parameters.AddWithValue("@uid", txt_User.Text);
            conn.Open();
            int count =(int)cmd.ExecuteScalar();
            SqlDataReader rdr = cmd2.ExecuteReader();
            while (rdr.Read())
            {
             string Email=rdr["EmailId"].ToString();
             Session["Email"] = Email;
            }
            Session["User"] = txt_User.Text;          
            
            conn.Close();
            if (count > 0)
            {                
                
                Response.Redirect("PlanMyTravel.aspx");             
               
      
            }
            else
            {
                lbl_LoginError.Text = "Please enter valid credentials.If you forgot your password please click on forgot password link below";
                lbl_LoginError.ForeColor= System.Drawing.Color.Red;
                
            
            }       
            

       }

        public string display(int a)
        {
            return "sample";
        }

        public int Retrieve(int b)
        {
            return b;
        }


        

        
    }
}