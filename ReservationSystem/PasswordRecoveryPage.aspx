﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="PasswordRecoveryPage.aspx.cs" Inherits="ReservationSystem.RecoveryClass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
    <style type="text/css">
        .style4
        {
            width: 272px;
            text-align: right;
        }
        .style5
        {
            color: #663300;
            text-align: center;
            width: 411px;
        }
        .style6
        {
            text-align: center;
            width: 411px;
        }
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <table style="width:100%;">
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style5">
                <strong>Password Recovery Page</strong></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style6">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                <strong>Enter your User Id&nbsp;&nbsp; :</strong></td>
            <td class="style6">
                <asp:TextBox ID="txt_RUserId" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                <strong>Enter your registered E-Mail Id&nbsp;&nbsp; :</strong></td>
            <td class="style6">
                <asp:TextBox ID="txt_REmail" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style6">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style6">
                <asp:Button ID="btn_Recover" runat="server" Text="Recover" 
                    onclick="btn_Recover_Click" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style6">
                <asp:Button ID="btn_back" runat="server" onclick="btn_back_Click" Text="Back" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style6">
                Your Password is :
                <asp:Label ID="lbl_RPassword" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    
</asp:Content>
