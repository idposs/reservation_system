﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace ReservationSystem
{
    public partial class RecoveryClass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Recover_Click(object sender, EventArgs e)
        {
            
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["ReservationDBConnectionString"].ConnectionString;
            string query = "select Password from Registration where UserId=@uid and EmailId=@eid";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@uid", txt_RUserId.Text);
            cmd.Parameters.AddWithValue("@eid", txt_REmail.Text);
            conn.Open();
            SqlDataReader rdr=cmd.ExecuteReader();
            while (rdr.Read())
                lbl_RPassword.Text = rdr["Password"].ToString();

            
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

       
        
    }
}