﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="PlanMyTravel.aspx.cs" Inherits="ReservationSystem.PlanClass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style4
        {
            width: 357px;
            text-align: center;
            height: 22px;
        }
        .style9
        {
            width: 278px;
            text-align: right;
        }
        .style11
        {
            width: 282px;
            text-align: right;
        }
        .style12
        {
            width: 285px;
            text-align: right;
        }
        .style13
        {
            width: 557px;
            text-align: center;
        }
        .style14
        {
            font-size: medium;
        }
        .style22
        {
            text-align: center;
            height: 22px;
        }
        .style23
        {
            width: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      
        <table style="width:100%; height: 27px;">
            <tr>
                <td class="style22" bgcolor="#FFCC00">
                    <asp:LinkButton runat="server" ID="lb_Book" onclick="lb_Book_Click"> Book Ticket</asp:LinkButton>
                </td>
                <td class="style22" bgcolor="#FFCC00">
                    &nbsp;<asp:LinkButton ID="lb_Cancel" runat="server" onclick="lb_Cancel_Click" 
                        style="font-size: large">Cancel Ticket</asp:LinkButton>
                </td>
                <td class="style22" bgcolor="#FFCC00">
                    &nbsp;<asp:LinkButton 
                        ID="lb_Profile" runat="server" onclick="lb_Profile_Click" 
                        style="font-size: large">View Profile</asp:LinkButton>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td class="style22" bgcolor="#FFCC00">
                    <asp:LinkButton ID="lb_SignOut" runat="server" onclick="lb_SignOut_Click" 
                        style="font-size: large">SignOut</asp:LinkButton>
                </td>
            </tr>
           
        </table>
    
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span 
            class="style14"><strong> 
        WELCOME</strong></span>
        <asp:Label ID="lbl_UserLogged" runat="server" Font-Italic="False" 
            ForeColor="#0066FF"></asp:Label>
</p>
    <p style="height: 288px; width: 1105px">
        <asp:Image ID="Image2" runat="server" Height="273px" 
            ImageUrl="~/Images/train.jpg" Width="1108px" ImageAlign="Middle" 
            style="margin-top: 0px"/>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Panel ID="pan_BookTicket" runat="server" Visible="False">
            <table style="width:100%; background-color: #66FFCC;">
                <tr>
                    <td class="style9">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>From Station&nbsp;&nbsp;&nbsp;&nbsp; :</strong>&nbsp;</td>
                    <td class="style1">
                        <asp:DropDownList ID="dd_From" runat="server" DataSourceID="SqlDataSource1" 
                            DataTextField="Places" DataValueField="Places" Height="18px">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:ReservationDBConnectionString %>" 
                            SelectCommand="SELECT [Places] FROM [FromTo]"></asp:SqlDataSource>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>To Station&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;</strong></td>
                    <td class="style1">
                        <asp:DropDownList ID="dd_To" runat="server" DataSourceID="SqlDataSource2" 
                            DataTextField="Places" DataValueField="Places">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:ReservationDBConnectionString %>" 
                            SelectCommand="SELECT [Places] FROM [FromTo]"></asp:SqlDataSource>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                    <td class="style1">
                        <asp:Button ID="btn_Book0" runat="server" onclick="btn_Book_Click" 
                            Text="Book Ticket" />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style9">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                    <td class="style1">
                        <asp:Label ID="lbl_ManualError" runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:Panel>  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       
        <asp:Panel ID="pan_CancelTicket" runat="server" Visible="False">            
            <table style="width:100%; height: 107px; margin-top: 0px; background-color: #66FFCC;">
                <tr>
                    <td class="style11">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&nbsp;Enter Your Ticket Number&nbsp;&nbsp; :</strong></td>
                    <td class="style1">
                        <asp:TextBox ID="txt_CTicketNumber" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style11">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&nbsp;&nbsp;&nbsp; Enter Your Email Id&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</strong></td>
                    <td class="style1">
                        <asp:TextBox ID="txt_CEmailId" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style11">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                    <td class="style1">
                        <asp:Label ID="lbl_CancelMessage" runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="style11">
                        &nbsp;</td>
                    <td class="style1">
                        <asp:Button ID="btn_Cancel0" runat="server" onclick="btn_Cancel_Click" 
                            Text="Cancel Ticket" />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>           
            </asp:Panel>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Panel ID="pan_Profile" runat="server" Visible="false">
                <table style="width:100%; background-color: #66FFCC;">
                    <tr>
                        <td class="style12">
                            <strong>User Id&nbsp;&nbsp; :</strong></td>
                        <td class="style13">
                            <asp:Label ID="lbl_PUser" runat="server"></asp:Label>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style12">
                            <strong>E-Mail Id&nbsp;&nbsp; :</strong></td>
                        <td class="style13">
                            <asp:Label ID="lbl_PEmail" runat="server"></asp:Label>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style12">
                            &nbsp;</td>
                        <td class="style13">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
        
        </asp:Content>
