﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Globalization;

namespace ReservationSystem
{
    public partial class PlanClass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string loggeduser = (string)Session["User"];
                lbl_UserLogged.Text = loggeduser.ToUpper(CultureInfo.InvariantCulture) + "!!";
                
            }
        }


        protected void lb_Book_Click(object sender, EventArgs e)
        {
            pan_CancelTicket.Visible = false;
            pan_Profile.Visible = false;
            pan_BookTicket.Visible = true;

        }

        protected void lb_Cancel_Click(object sender, EventArgs e)
        {
            pan_BookTicket.Visible = false;
            pan_Profile.Visible = false;
            pan_CancelTicket.Visible = true;

        }        

        protected void lb_Profile_Click(object sender, EventArgs e)
        {
            pan_CancelTicket.Visible = false;
            pan_BookTicket.Visible = false;
            pan_Profile.Visible = true;
            lbl_PUser.Text = (string)Session["User"];
            lbl_PEmail.Text = (string)Session["Email"];


        }

        protected void btn_Book_Click(object sender, EventArgs e)
        {

            if (dd_To.SelectedItem.Text == dd_From.SelectedItem.Text)
            {
                lbl_ManualError.Text = "From Station and To Station should be different";
                lbl_ManualError.ForeColor = Color.Red;
            }
            else
            Response.Redirect("Reservation.aspx?From=" +dd_From.SelectedItem.Text + "&To=" +dd_To.SelectedItem.Text);

        }

        protected void btn_Cancel_Click(object sender, EventArgs e)
        {
            txt_CTicketNumber.Text = txt_CEmailId.Text = null;
            lbl_CancelMessage.Text = "Ticket has been cancelled";
        }

        protected void lb_SignOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx?event="+lb_SignOut.Text);
        }

        





    }
}