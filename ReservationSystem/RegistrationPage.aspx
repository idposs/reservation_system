﻿<%--<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="RegistrationPage.aspx.cs" Inherits="ReservationSystem.RegistrationClass" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
     function compare() {

//              Example of how to access asp.net server control in java script by using "client id mode" property with values "auto id" and "static".
//              The default value of "client id mode" property is auto id

         var x = document.getElementById('<%=txt_RegPassword.ClientID%>').value;
         var y = document.getElementById('txt_RegPasswordRetype').value;         
         if (x != y) 
         {             
             alert("Password Mismatch");                         
         }
         
     }
        
    </script>
    
    <style type="text/css">
        .style4
        {
            width: 312px;
            text-align: right;
        }
        .style5
        {
            width: 305px;
            text-align: center;
        }
        .style6
        {
            width: 305px;
            text-align: center;
            color: #663300;
        }
        .style7
        {
            width: 312px;
            font-size: large;
            text-align: right;
        }
        .style8
        {
            width: 305px;
            text-align: right;
        }         

    </style>
    

}

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    
    <table style="width: 100%; height: 355px;">
        <tr>
            <td class="style7">
                &nbsp;</td>
            <td class="style6">
                <strong>Registration Page</strong></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style7">
                &nbsp;</td>
            <td class="style5">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style7">
                <strong>Desired User Id&nbsp;&nbsp; :</strong></td>
            <td class="style5">
                <strong>
    <asp:TextBox ID="txt_RegUser" runat="server"></asp:TextBox>
                </strong>
            </td>
            <td>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ControlToValidate="txt_RegUser" ErrorMessage="Username Can not be blank" 
        ForeColor="Red" ValidationGroup="vg-reg"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <strong>Password&nbsp;&nbsp; :</strong></td>
            <td class="style5">
        <asp:TextBox ID="txt_RegPassword" runat="server" TextMode="Password" ClientIDMode="AutoID"></asp:TextBox>
            </td>
            <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="txt_RegPassword" ErrorMessage="Password can not be blank" 
            ForeColor="Red" ValidationGroup="vg-reg"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <strong>Re-Type Password&nbsp;&nbsp; :</strong></td>
            <td class="style5">
        <asp:TextBox ID="txt_RegPasswordRetype" runat="server" TextMode="Password" 
                    ClientIDMode="Static"></asp:TextBox>
                
            </td>
            <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
            ControlToValidate="txt_RegPasswordRetype" 
            ErrorMessage="Please re-confim the password" ForeColor="Red" 
            ValidationGroup="vg-reg"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style7">
                <strong>Your E-Mail Id&nbsp;&nbsp; :</strong></td>
            <td class="style5">
        <asp:TextBox ID="txt_RegEmail" runat="server"></asp:TextBox>
            </td>
            <td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
            ControlToValidate="txt_RegEmail" ErrorMessage="Email id cant be blank" 
            ForeColor="Red" ValidationGroup="vg-reg" Display="Dynamic"></asp:RequiredFieldValidator>
            &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                    runat="server" ControlToValidate="txt_RegEmail" 
                    ErrorMessage="The format should be xxx@yyy.com" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                    ValidationGroup="vg-reg"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="style8">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td class="style5">
        <asp:Button ID="btn_Register" runat="server" onclick="btn_Register_Click" 
            Text="Register" ValidationGroup="vg-reg"/>
                <br />
                <br />
                <asp:Button ID="btn_back" runat="server" onclick="btn_back_Click" Text="Back" />
            </td>
            <td>
                <asp:Label ID="lbl_Registered" runat="server"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td class="style7">
                &nbsp;</td>
            <td class="style5">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    
    
</asp:Content>
