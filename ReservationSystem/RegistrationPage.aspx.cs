﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Resources;
using System.Reflection;
using System.Configuration;

namespace ReservationSystem
{
    public partial class RegistrationClass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            //If you want to add html events to the server side controls use the following syntax
            txt_RegPasswordRetype.Attributes.Add("onchange", "compare()");
        }

        protected void btn_Register_Click(object sender, EventArgs e)
        {
                ResourceManager rm = new ResourceManager("Strings", Assembly.GetExecutingAssembly());
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["ReservationDBConnectionString"].ConnectionString;
                string query = "insert into dbo.Registration values (@RegUser,@RegPwd,@RegMail)";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@RegUser", txt_RegUser.Text);
                cmd.Parameters.AddWithValue("@RegPwd", txt_RegPassword.Text);
                cmd.Parameters.AddWithValue("@RegMail", txt_RegEmail.Text);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                txt_RegUser.Text = txt_RegEmail.Text = null;
                lbl_Registered.Text = "Registration Successfull";
                //lbl_Registered.Text = rm.GetString("successful");
            
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}
