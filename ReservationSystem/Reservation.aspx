﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Reservation.aspx.cs" Inherits="ReservationSystem.ReservationClass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style4
        {
            width: 71px;
            text-align: center;
        }
        .style10
        {
            width: 309px;
            text-align: right;
        }
        .style11
        {
            width: 232px;
            text-align: center;
        }
        .style12
        {
            color: #0000FF;
            width: 220px;
        }
        .style13
        {
            width: 220px;
        }
        .style14
        {
            width: 309px;
            text-align: right;
            height: 32px;
        }
        .style15
        {
            width: 232px;
            text-align: center;
            height: 32px;
        }
        .style16
        {
            width: 220px;
            height: 32px;
        }
        .style17
        {
            height: 32px;
        }
        .style19
        {
            font-family: Arial, Helvetica, sans-serif;
            height: 22px;
            width: 163px;
            text-align: center;
        }
        .style20
        {
            font-size: large;
            height: 22px;
            width: 17px;
            text-align: center;
        }
        .style21
        {
            width: 71px;
            height: 22px;
            text-align: center;
        }
        .style28
        {
            font-family: Arial, Helvetica, sans-serif;
            width: 163px;
            text-align: center;
        }
        .style30
        {
            height: 22px;
            text-align: center;
        }
        .style32
        {
            text-align: center;
            height: 25px;
        }
        .style33
        {
            font-family: Arial, Helvetica, sans-serif;
            width: 163px;
            height: 25px;
            text-align: center;
        }
        .style34
        {
            font-size: large;
            height: 25px;
            width: 17px;
            text-align: center;
        }
        .style35
        {
            width: 71px;
            text-align: center;
            height: 25px;
        }
        .style36
        {
            font-size: large;
            width: 17px;
            text-align: center;
        }
        .style44
        {
            width: 163px;
            text-align: center;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <table style="width: 100%; background-image: url('Images/train.jpg'); background-repeat: repeat;" 
        cellpadding="1" cellspacing="1" >
            <tr>
                <td class="style44">
                    <strong>From</strong></td>
                <td class="style44">
                    <strong>To</strong></td>
                <td class="style44">
                    <strong>Distance</strong></td>
                <td class="style44">
                    <strong>Fare</strong></td>
                <td class="style44">
                    <strong>TrainName</strong></td>
            </tr>
            <tr>
                <td class="style44">
                    <asp:Label ID="lbl_From" runat="server"></asp:Label>
                </td>
                <td class="style44">
                    <asp:Label ID="lbl_To" runat="server"></asp:Label>
                </td>
                <td class="style44">
                    <asp:Label ID="lbl_Distance" runat="server" style="text-align: right"></asp:Label>
                </td>
                <td class="style44">
                    <asp:Label ID="lbl_Fare" runat="server"></asp:Label>
                </td>
                <td class="style44">
                    <asp:Label ID="lbl_TrainName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style44">
                    &nbsp;</td>
                <td class="style44">
                    &nbsp;</td>
                <td class="style36">
                    &nbsp;</td>
                <td class="style4">
                    &nbsp;</td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;</td>
                <td class="style28">
                    &nbsp;</td>
                <td class="style36" rowspan="1">
                    &nbsp;</td>
                <td class="style1">
                    &nbsp;</td>
                <td class="style1" colspan="8">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style32">
                    </td>
                <td class="style33">
                    </td>
                <td class="style34">
                    </td>
                <td class="style35">
                    </td>
                <td class="style32">
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;</td>
                <td class="style28">
                    &nbsp;</td>
                <td class="style44">
                    &nbsp;</td>
                <td class="style4">
                    &nbsp;</td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style30">
                    </td>
                <td class="style19">
                    </td>
                <td class="style20">
                    </td>
                <td class="style21">
                    </td>
                <td class="style30">
                    </td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;</td>
                <td class="style28">
                    &nbsp;</td>
                <td class="style36">
                    <asp:Button ID="btn_Proceed" runat="server" Text="Proceed to payment" 
                        onclick="btn_Proceed_Click" style="margin-left: 23px" />
                </td>
                <td class="style4">
                    <asp:Button ID="lbl_Replan" runat="server" onclick="lbl_Replan_Click" 
                        Text="Re-Plan my travel" />
                </td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
        </table>
    <p></p>
    <asp:Panel ID="pan_PG" runat="server" Height="317px" Visible="False" 
        BorderStyle="Solid">
        <table style="width:100%; height: 315px; background-color: #66FFCC;" 
            bgcolor="#CCFFFF" id="tbl_PG">
            <tr>
                <td class="style14">
                    </td>
                <td class="style15">
                    <strong>Payment Gateway</strong></td>
                <td class="style16">
                    </td>
                <td class="style17">
                    <asp:LinkButton ID="lb_SignOut" runat="server" onclick="lb_SignOut_Click" 
                        style="font-size: large">SignOut</asp:LinkButton>
                    
                    </td>
            </tr>
            <tr>
                <td class="style10">
                    &nbsp;</td>
                <td class="style11">
                    &nbsp;</td>
                <td class="style13">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style10">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Type of card&nbsp; :</strong></td>
                <td class="style11">
                    <asp:DropDownList ID="dd_TypeOfCard" runat="server" Height="22px" Width="132px">
                        <asp:ListItem Value="1">Visa</asp:ListItem>
                        <asp:ListItem Value="2">Master</asp:ListItem>
                        <asp:ListItem Value="3">Maestro</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style13">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style10">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Name on the card&nbsp; :</strong></td>
                <td class="style11">
                    <asp:TextBox ID="txt_NameOnCard" runat="server"></asp:TextBox>
                </td>
                <td class="style13">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txt_NameOnCard" ErrorMessage="Name on the card is mandatory" 
                        ForeColor="Red" ValidationGroup="PG"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style10">
                    <strong>Expiry Date(DDMM Format) :</strong></td>
                <td class="style11">
                    <asp:TextBox ID="txt_Expiry" runat="server"></asp:TextBox>
                </td>
                <td class="style12">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txt_Expiry" Display="Dynamic" 
                        ErrorMessage="Expiry date is mandatory" ForeColor="Red" ValidationGroup="PG"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style10">
                    <strong>CVV:</strong></td>
                <td class="style11">
                    <asp:TextBox ID="txt_Cvv" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td class="style13">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txt_Cvv" ErrorMessage="CVV number is mandatory" 
                        ForeColor="Red" ValidationGroup="PG"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Label ID="lbl_Successfully" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style10">
                    &nbsp;</td>
                <td class="style11">
                    <asp:Button ID="btn_Pay" runat="server" Text="Pay" ValidationGroup="PG" 
                        onclick="btn_Pay_Click"/>
               </td>
                <td class="style13">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style10">
                    &nbsp;</td>
                <td class="style11">
                    &nbsp;</td>
                <td class="style13">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </asp:Panel>
    <p></p>

</asp:Content>
