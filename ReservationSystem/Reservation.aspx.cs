﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace ReservationSystem
{
    public partial class ReservationClass : System.Web.UI.Page
    {


        /// <summary>
        /// Page load method comments
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                
                string from=Request.QueryString["From"];
                string to = Request.QueryString["To"];
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["ReservationDBConnectionString"].ConnectionString;
                string query = "select * from Reservation where FromPlace=@fromplace and ToPlace=@toplace";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@fromplace", from);
                cmd.Parameters.AddWithValue("@toplace", to);
                conn.Open();
               
                SqlDataReader rdr= cmd.ExecuteReader();
                while (rdr.Read())
                {
                    lbl_From.Text = rdr["FromPlace"].ToString();
                    lbl_To.Text = rdr["ToPlace"].ToString();
                    lbl_Distance.Text = rdr["Distance"].ToString();
                    lbl_Fare.Text = rdr["Fare"].ToString();
                    lbl_TrainName.Text = rdr["TrainName"].ToString();                 
                
                }


            }
        }

        protected void btn_Proceed_Click(object sender, EventArgs e)
        {
            pan_PG.Visible = true;
        }

        protected void btn_Pay_Click(object sender, EventArgs e)
        {

            txt_NameOnCard.Text = txt_Expiry.Text = null;
            lbl_Successfully.Text = "Ticket has been successfully booked.Your ticket number is 5659.Please check your e-mail for e-ticket";
        }

        protected void lbl_Replan_Click(object sender, EventArgs e)
        {
            Response.Redirect("PlanMyTravel.aspx");
        }

        protected void lb_SignOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx?event=" + lb_SignOut.Text);
        }
    }
}