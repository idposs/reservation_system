package com.example.tests;

import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NodeTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new InternetExplorerDriver();
    baseUrl = "http://sparshv2/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);


  }

  @Test
  public void testNode() throws Exception {
    driver.get(baseUrl);
    WebDriverWait waitVar = new WebDriverWait(driver, 100);
    driver.get(baseUrl + "pages/Home.aspx");
    WebElement webappsLink = driver.findElement(By.linkText("Web Apps"));
    WebElement ahdLink = driver.findElement(By.xpath("//div[@id=\"WebApps\"]/table/tbody/tr/td"));

    Actions builder = new Actions(driver);
    Action mouseOver = builder.moveToElement(webappsLink).build();
    mouseOver.perform();
    
    
    waitVar.until(ExpectedConditions.elementToBeClickable(ahdLink));
    
    try {
          Thread.sleep(1000);
    } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
    }
    
    ahdLink.click();
    ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles()); 
    driver.switchTo().window(tabs.get(1));
    System.out.println(driver.getTitle());          
    
    driver.switchTo().frame(driver.findElement(By.xpath("//*[@id=\"mainFrameSet\"]/frame[2]")));////*[@id="roles"]
    ////*[@id="mainFrameSet"]/frame[2]
    
    WebElement roleTab = driver.findElement(By.xpath("//select[@id=\"roles\"]"));
    
    roleTab.click();
    WebElement empRole = driver.findElement(By.xpath("//select[@id=\"roles\"]/option[@value=\"10005\"]"));
    WebDriverWait waitVar2 = new WebDriverWait(driver, 3);
    waitVar2.until(ExpectedConditions.elementToBeClickable(empRole));
    empRole.click();
    driver.findElement(By.xpath("//a[@id=\"imgBtn0\"]")).click();
    
    driver.switchTo().frame(driver.findElement(By.xpath("//*[@id=\"mainFrameSet\"]/frame[3]")));
    
    driver.findElement(By.xpath("/html/body/div[3]/div/div/div[2]/div/div[2]/a")).click();
   
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

}

  

